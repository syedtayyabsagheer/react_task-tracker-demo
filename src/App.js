import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';

import Header from './components/Header';
import Task from './components/Task';
import AddTask from './components/AddTask';
function App() {
  const [tasks,setTasks] = useState([
    {
    id: 1,
    title: "Learn GRAPHQL",
    description: "YEs this weak i will to it",
    remainder: true,
},
{
    id: 2,
    title: "Learn ror",
    description: "Yes this weak i will to it",
    remainder: true,
},
{
    id: 3,
    title: "Learn RAILS ",
    description: "YEs this weak i will to it",
    remainder: false,
},])
const [showAddForm,setAddForm]=useState(false);
const addTask = (task) => {
  const id = Math.floor(Math.random()* 100000) + 1;
  const newTask = {id,...task}
  setTasks([...tasks,newTask])
}
const deleteTask = (id) => {
  setTasks(tasks.filter((task)=> task.id !== id))
}

const toggleRemainder = (id,remainder) => {
  console.log(remainder);
  setTasks(tasks.map((task)=> task.id === id  ? task.remainder === true ? {...task,remainder:false} : {...task,remainder:true}:task  ))
}

  return (
    <React.Fragment>
          <Header setAddForm={setAddForm}/>
           <AddTask addTask={addTask}/> 
          <Task tasks={tasks} onDelete={deleteTask} onToggle={toggleRemainder}/>
    </React.Fragment>
  );
}

export default App;
