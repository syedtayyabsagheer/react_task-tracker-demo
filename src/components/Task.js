import { FaWindowClose} from "react-icons/fa";
const Task = ({tasks,onDelete,onToggle}) => {
    return(
        <>
            {tasks.length > 0 ? tasks.map((task)=>(<div className="task_style" onDoubleClick={()=>onToggle(task.id,task.remainder)} ><h4>{task.title}<span onClick={()=>onDelete(task.id)}><FaWindowClose style={{cursor: 'pointer'}}/></span></h4>
            <p>{task.description}</p><h5>Remainder: {task.remainder === true ? "Yes" : "No"}</h5></div>)) : 'Sorry for inconvince'}
        </>
    );
}

export default Task;