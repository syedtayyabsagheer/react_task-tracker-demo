import { useState } from 'react';
import Button from './Button';
const Header = ({title,setAddForm}) => {
    return (
    <header>
        <Button color='red' text='Add' type="button" setAddForm={setAddForm} />
        <h1>{title}</h1>
    </header>
    );
}

Header.defaultProps = {
    title: 'Task Tracker'
}


export default Header;