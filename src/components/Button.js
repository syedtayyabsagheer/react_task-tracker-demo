import { useState } from "react";

const Button = ({color,text,type,customclassName,setAddForm}) => {
    return(
        
       <button onClick={ ()=> setAddForm(true)}  className={customclassName} type={type} style={{backgroundColor:color}}>{text}</button> 
    )
}

export default Button;