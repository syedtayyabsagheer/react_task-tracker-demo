import Button from "./Button"
import {useState} from "react"

const AddTask = ({addTask}) => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [remainder, setRemainder] = useState(false);

const onSubmit = (e) => {
    e.preventDefault();

    if(!title)
    {
        alert("please add the title");
        return
    }
    if(!description)
    {
        alert("please add the description of the title")
        return
    }

    addTask({title,description,remainder});
    setRemainder(false);
    setTitle('');
    setDescription('');
}
    return (
        <form onSubmit={onSubmit}>
            <h1>Add Task to Task Tracker</h1>
            <div className="form-control">
                <label>Task Title</label>
                <input type="text" placeholder="Title" className="taskttitle" value={title} onChange={(e) => setTitle(e.target.value)}></input>
            </div>
            <div className="form-control">
                <label>Task Description</label>
                <textarea placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)}>
                </textarea>
            </div>
            <div className="form-control">
                <label>Set remainder</label>
                <input type="checkbox" checked={remainder} className="taskdescription" value={remainder} onChange={(e) => setRemainder(e.currentTarget.checked)}></input>
            </div>
            <div className="form-control">
            <Button type="submit" color="blue" text="submit" customclassName="formbtn"></Button>
            </div>
        </form>
    )
}

export default AddTask;